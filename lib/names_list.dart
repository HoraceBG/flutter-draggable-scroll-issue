import 'package:draggable_scrollbar/draggable_scrollbar.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class NamesListView extends StatefulWidget {
  final List<String> names;
  final Key key;

  NamesListView(this.names)
      : this.key = Key('${names.length}');

  @override
  State<StatefulWidget> createState() => _NamesListViewState();
}

class _NamesListViewState extends State<NamesListView> {
  String gender;
  List<String> names;

  ScrollController _semicircleController;

  @override
  void initState() {
    super.initState();
    this.names = widget.names;
    this._semicircleController = ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    return DraggableScrollbar.semicircle(
        labelConstraints:
        BoxConstraints.tightFor(width: 80.0, height: 30.0),
        labelTextBuilder: (offset) {
          final int currentItem = offset ~/ 50.0;
          var letter = names[currentItem].substring(0, 1);
          return Text(
            "$letter",
            style: TextStyle(color: Colors.black),
          );
        },
        child: ListView.builder(
            controller: _semicircleController,
            itemCount: names.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(title: Text('${names[index]}'));
            }),
        controller: _semicircleController);
  }

  List<String> getFilteredNames(
      NameListDisplayOptions model, List<String> names) {
    if (noSearchFilters(model)) {
      return names;
    }

    return names.where((i) => i.startsWith(model.filter.searchQuery)).toList();
  }

  bool noSearchFilters(NameListDisplayOptions model) =>
      model == null || model.filter == null || model.filter.searchQuery == null;
}

class NameListDisplayOptions extends Model {
  SortOption _sortOption;
  Filter _filter;

  SortOption get sortOption => _sortOption;

  Filter get filter => _filter;

  void updateFilters(Filter filter) {
    this._filter = filter;
    notifyListeners();
  }

  void updateSortOptions(SortOption sortOption) {
    this._sortOption = sortOption;
    notifyListeners();
  }
}

/// Describes the filters displayed on a list of names.
class Filter {
  final String searchQuery;
  final List<NameCondition> nameConditions;

  Filter(this.searchQuery, this.nameConditions);
}

class NameCondition {
  static const STARTS_WITH = "starts_with";
  static const ENDS_WITH = "ends_with";

  final String key;
  final String value;

  NameCondition(this.key, this.value);
}

/// Describes the sorting ways in which a list of names
/// can be filtered.
class SortOption {
  static const ALPHABETICAL = "alphabetical";
  static const REVERSE_ALPHABETICAL = "reverse_alphabetical";
  static const MOST_POPULAR = "most_popular";
  static const LEAST_POPULAR = "least_popular";

  final String sortingOption;

  SortOption(this.sortingOption);
}
